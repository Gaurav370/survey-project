import { Component, OnInit } from '@angular/core';
import data from "../../../assets/data.json";
import { DataService } from 'src/app/Components/data.service';

@Component({
  selector: 'app-checkbox',
  templateUrl: './checkbox.page.html',
  styleUrls: ['./checkbox.page.scss'],
})
export class CheckboxPage implements OnInit {
  option:any;
  text:any;
  other:any;
  NOTA:any;
  constructor(private DataService: DataService) {
   }

  funmap( ind = this.DataService.index){
    this.other=data.questions[ind].other;
    this.NOTA=data.questions[ind].NOTA;
    var opt=data.questions[ind].options.map(function(x){ return x; });

    if(data.questions[ind].randomize=="true"){
      opt.sort(function(a,b){return 0.00002 - Math.random()});
      this.option= opt;
      }
  
    else{
      this.option=opt;}

    var txt=data.questions.map(function(x){ return x.questiontext; });
    this.text=txt[ind];
    console.log(ind);
  }

  ngOnInit() {
  }

}
