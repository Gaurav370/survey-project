import { Component, OnInit } from '@angular/core';
import data from "../../../assets/data.json";
import { DataService } from 'src/app/Components/data.service';
import { Inject }  from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { Location } from '@angular/common';


@Component({
  selector: 'app-radio',
  templateUrl: './radio.page.html',
  styleUrls: ['./radio.page.scss'],
})


export class RadioPage implements OnInit {
  option:any;
  text:any;
  other:any;
  NOTA:any;
  

  constructor(private DataService: DataService, @Inject(DOCUMENT) document, private location: Location) {   
   }
    
  
   funmap(ind = this.DataService.index){
    
    this.other=data.questions[ind].other;
    this.NOTA=data.questions[ind].NOTA;
     //console.log(this.DataService.index);
    var opt= data.questions[ind].options.map(function(x){return x;})

    if(data.questions[ind].randomize=="true"){
      opt.sort(function(a,b){return 0.00002 - Math.random()});
      this.option= opt;
      }

    else{
      this.option=opt;}

    var txt=data.questions.map(function(x){return x.questiontext;})
    this.text=txt[ind];
     }
  


  ngOnInit() {
    //console.log(this.DataService.index);
  }

}


