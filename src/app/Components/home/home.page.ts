import { Component } from '@angular/core';
//import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
//import { map, filter, switchMap } from 'rxjs/operators';
import data from "../../../assets/data.json";
import { DataService } from '../data.service';
 
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage { 

 
  constructor(private router: Router,private DataService: DataService) { 
    
  }


  async start()
  {
    var text = '0';
      localStorage.setItem('index', text);
// console.log(localStorage.getItem('index'));
    switch(data.questions[0].type) {
      case 'radio':
        this.router.navigate(['/radio']);
        break;

      case 'checkbox':
        this.router.navigate(['/checkbox']);
        break;

      case 'openend':
        this.router.navigate(['/openend']);
        break;

      case 'grid-radio':
        this.router.navigate(['/grid']);
        break;

      default:
        console.log(" Khatam, Bye bye, Tata, Good Bye, Gaya");
    }
  }

  // setQuestion(i) {
  //   const a = data.questions[i];
  //   console.log(a);
  // }
   
  // nextQuestion() {
  //   this.currentIndex++;
  //   this.setQuestion(this.currentIndex);
  //   this.data = this.data2.questions[this.currentIndex];
  // }
  
   
  
    
  

}