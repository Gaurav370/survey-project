import { Injectable } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import data from'src/assets/data.json';
import { Location } from '@angular/common';


@Injectable({
  providedIn: 'root'
})

export class DataService {
global:any=2;
index:any;

  constructor(private router: Router, private location: Location) {
    this.funIndex();
     }

     funIndex(){
      if(this.location.path()=='/home' || this.index==0){ 
        this.index=0;
      }
      else{
        this.index=localStorage.getItem('index');
      }
      console.log("abcd"+this.index);
     }

     
 
    


  previous(){

    
    if(this.index>0){

      this.index--;
      
      var text=this.index;
      localStorage.setItem('index', text);
     
     switch(data.questions[this.index].type) {
        case 'radio':
          this.router.navigate(['/radio']);
          break;
  
        case 'checkbox':
          this.router.navigate(['/checkbox']);
          break;
  
        case 'openend':
          this.router.navigate(['/openend']);
          break;
  
        case 'grid-radio':
          this.router.navigate(['/grid']);
          break;
  
        default:
          console.log(" Khatam, Bye bye, Tata, Good Bye, Gaya");
      }
    }  
    
  }



  next(){
    

    if(this.index>=0){

      this.index++;

      var text=this.index;
      localStorage.setItem('index', text);
      
     switch(data.questions[this.index].type) {
        case 'radio':
          this.router.navigate(['/radio']);
          break;
  
        case 'checkbox':
          this.router.navigate(['/checkbox']);
          break;
  
        case 'openend':
          this.router.navigate(['/openend']);
          break;
  
        case 'grid-radio':
          this.router.navigate(['/grid']);
          break;
  
        default:
          console.log(" Khatam, Bye bye, Tata, Good Bye, Gaya");
      }
    }
  }

}
