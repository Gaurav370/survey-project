import { Component, OnInit } from '@angular/core';
import data from "../../../assets/data.json";
import { DataService } from 'src/app/Components/data.service';

@Component({
  selector: 'app-openend',
  templateUrl: './openend.page.html',
  styleUrls: ['./openend.page.scss'],
})
export class OpenendPage implements OnInit {
  text:any;
  constructor(private DataService: DataService) {
  }

  funmap(ind = this.DataService.index){
    var txt=data.questions.map(function(x){return x.questiontext;})
    this.text=txt[ind];
     }

  ngOnInit() {
  }

}
