import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OpenendPage } from './openend.page';

const routes: Routes = [
  {
    path: '',
    component: OpenendPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OpenendPageRoutingModule {}
