import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OpenendPageRoutingModule } from './openend-routing.module';

import { OpenendPage } from './openend.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OpenendPageRoutingModule
  ],
  declarations: [OpenendPage]
})
export class OpenendPageModule {}
