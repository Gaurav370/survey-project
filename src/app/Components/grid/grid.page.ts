import { Component, OnInit } from '@angular/core';
import data from "../../../assets/data.json";
import { DataService } from 'src/app/Components/data.service';

@Component({
  selector: 'app-grid',
  templateUrl: './grid.page.html',
  styleUrls: ['./grid.page.scss'],
})
export class GridPage{
  grid:any;
  other:any;

  constructor(private DataService: DataService) { }

  funmap(ind = this.DataService.index){
    this.other=data.questions[ind].other;
   this.grid=data.questions[ind];
  }

  ngOnInit() {
  }

}

