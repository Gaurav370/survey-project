import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import {AppComponent} from 'src/app/app.component';
import data from "../../../assets/data.json";



const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./components/home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'radio',
    loadChildren: () => import('./Components/radio/radio.module').then( m => m.RadioPageModule)
  },
  {
    path: 'checkbox',
    loadChildren: () => import('./Components/checkbox/checkbox.module').then( m => m.CheckboxPageModule)
  },
  {
    path: 'grid',
    loadChildren: () => import('./Components/grid/grid.module').then( m => m.GridPageModule)
  },
  {
    path: 'openend',
    loadChildren: () => import('./Components/openend/openend.module').then( m => m.OpenendPageModule)
  },
  {
    path: 'footer',
    loadChildren: () => import('./Components/footer/footer.module').then( m => m.FooterPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
